import * as sst from "@serverless-stack/resources";
import { CfnLedger, CfnStream } from "@aws-cdk/aws-qldb";
import * as KinesisCDK from "@aws-cdk/aws-kinesis";
import * as iam from "@aws-cdk/aws-iam";

export default class MyStack extends sst.Stack {
  constructor(scope: sst.App, id: string, props?: sst.StackProps) {
    super(scope, id, props);

    /*
     * *********** Ledger ****************
     */
    new CfnLedger(this, "fiorCoreLedger", {
      name: "core-ledger",
      permissionsMode: "ALLOW_ALL",
      deletionProtection: false,
    });

    /*
     * *********** Stream ****************
     */
    const kinesisDataStream = new KinesisCDK.Stream(
      this,
      "KinesisDataStreamForQldbStreaming",
      {
        shardCount: 1,
        // encryption: KinesisCDK.StreamEncryption.KMS,
        // encryptionKey: kdsKmsKey,
        streamName: "kinesis-stream",

        // retentionPeriod: Duration.hours(24),
      }
    );
    const qldbStreamRole = new iam.Role(this, "QldbStreamRole", {
      assumedBy: new iam.ServicePrincipal("qldb.amazonaws.com"),
      description:
        "Role used by QLDB to stream data into Kinesis Data Stream instance",
    });

    kinesisDataStream.grantWrite(qldbStreamRole);
    kinesisDataStream.grantRead(qldbStreamRole);
    kinesisDataStream.grant(qldbStreamRole, "kinesis:DescribeStream");

    const qldbLedgerStream = new CfnStream(this, "qldbCoreLedgerStream", {
      ledgerName: "core-ledger",
      streamName: "qldb-stream",
      roleArn: qldbStreamRole.roleArn,
      inclusiveStartTime: new Date().toISOString(),

      kinesisConfiguration: {
        aggregationEnabled: true,
        streamArn: kinesisDataStream.streamArn,
      },
    });

    qldbLedgerStream.node.addDependency(qldbStreamRole);
    qldbLedgerStream.node.addDependency(kinesisDataStream);
  }
}
